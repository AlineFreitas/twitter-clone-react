import React from 'react';
import {
  Avatar,
  Banner,
  CakeIcon,
  Container,
  EditProfileButton,
  LocationIcon,
  Network,
  ProfileData,
} from './style';

const ProfilePage: React.FC = () => {
  return (
    <Container>
      <Banner>
        <Avatar/>
      </Banner>
      <ProfileData>
        <EditProfileButton outlined>Edit Profile</EditProfileButton>
        <h1>Aline Freitas</h1>
        <h2>@alineofreitas</h2>

        <p>Developer <a href="https://www.thoughtworks.com/" target="_blank" rel="noreferrer noopener">@ThoughtWorks</a></p>
        <ul>
          <li>
            <LocationIcon/>
            Campos dos Goytacazes, RJ
          </li>
          <li>
            <CakeIcon />
            Nascida em 12 de setembro de 1990
          </li>
        </ul>

        <Network>
          <span>
            following <strong>100</strong>
          </span>
          <span>
            <strong>10</strong> followers
          </span>
        </Network>
      </ProfileData>
    </Container>
  )
}

export default ProfilePage;