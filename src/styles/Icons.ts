export {
  Home,
  Notifications,
  Email,
  LocationOn,
  Search,
} from 'styled-icons/material-outlined';
export {
  Cake,
} from 'styled-icons/material';
export { ArrowLeft} from 'styled-icons/heroicons-solid';