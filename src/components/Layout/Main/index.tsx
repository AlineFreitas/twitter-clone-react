import React from 'react';
import {
  Container,
  Header,
  BackIcon,
  ProfileInfo,
  BottonMenu,
  HomeIcon,
  SearchIcon,
  BellIcon,
  EmailIcon,
} from './style';

import Feed from '../../Feed';
import ProfilePage from '../../ProfilePage';

const Main: React.FC = () => {
  return (
    <Container>
      <Header>
        <button>
          <BackIcon />
        </button>

        <ProfileInfo>
          <strong>Aline Freitas</strong>
          <span>124 tweets</span>
        </ProfileInfo>
      </Header>

      <ProfilePage />

      <Feed />

      <BottonMenu>
        <HomeIcon />
        <SearchIcon />
        <BellIcon />
        <EmailIcon />
      </BottonMenu>
    </Container>
  )
}

export default Main;