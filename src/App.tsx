import React from 'react';
import GlobalStyles from './styles/GlobalStyle'

import Layout from './components/Layout';

function App() {
  return (
    <div>
      <Layout />
      <GlobalStyles/>
    </div>
  );
}

export default App;